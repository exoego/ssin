function nf( n ) {
  if ( Math.abs( n ) < 1.0 ) {
    let e = parseInt( n.toString().split( 'e-' )[1] )
    if ( e ) {
      n *= Math.pow( 10, e-1 )
      n = `0.` + ( new Array( e ) ).join( `0` ) + n.toString().substring( 2 )
    }
  } else {
    let e = parseInt( n.toString().split( `+` )[1] )
    if ( e > 20 ) {
      e -= 20
      n /= Math.pow( 10, e )
      n += ( new Array( e + 1 ) ).join( `0` )
    }
  }
  return String( n )
}

function getNum( _ns, nod, dp ) {
  const n = _ns.reduce( ( acc, cur, i ) => {
    if ( i < nod ) {
      return acc + cur
    }
    return acc
  } )

  const ltdp = _ns.reduce( ( acc, cur, i ) => {
    if ( i > nod ) {
      return acc + cur
    }
    return cur
  } )

  return `${n}${dp > 0 ? '.' : ''}${ltdp.slice(0, dp)}`
}

function ssin( n, decimalPoint = 0 ) {
  const _n = nf( n )
  const _ns = _n.split('')
  const _nl = _n.length

  if ( _nl > 6 ) {
    if ( _nl < 10 ) {
      return [getNum( _ns, _nl - 6, decimalPoint ), `million`]
    } else if( _nl < 13 ) {
      return [getNum( _ns, _nl - 9, decimalPoint ), `billion`]
    } else if ( _nl < 16 ) {
      return [getNum( _ns, _nl - 12, decimalPoint ), `trillion`]
    } else if ( _nl < 19 ) {
      return [getNum( _ns, _nl - 15, decimalPoint ), `quadrillion`]
    } else if ( _nl < 22 ) {
      return [getNum( _ns, _nl - 18, decimalPoint ), `quintillion`]
    } else if ( _nl < 25 ) {
      return [getNum( _ns, _nl - 21, decimalPoint ), `sextillion`]
    } else if ( _nl < 28 ) {
      return [getNum( _ns, _nl - 24, decimalPoint ), `septillion`]
    } else if ( _nl < 31 ) {
      return [getNum( _ns, _nl - 27, decimalPoint ), `octillion`]
    } else if ( _nl < 34 ) {
      return [getNum( _ns, _nl - 30, decimalPoint ), `nonillion`]
    } else if ( _nl < 37 ) {
      return [getNum( _ns, _nl - 33, decimalPoint ), `decillion`]
    } else if ( _nl < 40 ) {
      return [getNum( _ns, _nl - 36, decimalPoint ), `undecillion`]
    } else if ( _nl < 43 ) {
      return [getNum( _ns, _nl - 39, decimalPoint ), `duodecillion`]
    } else if ( _nl < 46 ) {
      return [getNum( _ns, _nl - 42, decimalPoint ), `tredecillion`]
    } else if ( _nl < 49 ) {
      return [getNum( _ns, _nl - 45, decimalPoint ), `quattuordecillion`]
    } else if ( _nl < 52 ) {
      return [getNum( _ns, _nl - 48, decimalPoint ), `quindecillion`]
    } else if ( _nl < 55 ) {
      return [getNum( _ns, _nl - 51, decimalPoint ), `sexdecillion`]
    } else if ( _nl < 58 ) {
      return [getNum( _ns, _nl - 54, decimalPoint ), `septendecillion`]
    } else if ( _nl < 61 ) {
      return [getNum( _ns, _nl - 57, decimalPoint ), `septendecillion`]
    } else if ( _nl < 64 ) {
      return [getNum( _ns, _nl - 60, decimalPoint ), `octodecillion`]
    } else if ( _nl < 67 ) {
      return [getNum( _ns, _nl - 63, decimalPoint ), `novemdecillion`]
    } else if ( _nl < 70 ) {
      return [getNum( _ns, _nl - 66, decimalPoint ), `vigintillion`]
    } else if ( _nl < 73 ) {
      return [getNum( _ns, _nl - 69, decimalPoint ), `centillion`]
    }
  }
  return [nf(n), ``]
}

module.exports = ssin
